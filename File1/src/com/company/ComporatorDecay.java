package com.company;

import java.util.Comparator;

//для того, чтобы объекты можно было отсортировать применяем интерфейс Comparable,
// в котором создаем свое правило для сортировки
public class ComporatorDecay implements Comparator<Integer> {

    //переопределение метода для создания своего правила сортировки
    @Override
    public int compare(Integer k1, Integer k2) {
        return k2-k1;
    }
}


