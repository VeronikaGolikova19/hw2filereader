package com.company;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class File1 {
    public static void main(String[] args) {

        //вычитка файла
        String line = new String();
        try {
            Scanner sc = new Scanner(new FileReader(new File("C:\\Users\\Ni4ka\\Documents\\number.txt")));
            line = sc.nextLine();

            //вспомогательный просмотр вычитанного файла
            System.out.println("Исходный файл: ");
            System.out.println(line + "\n");

        } catch (IOException ex) {
            System.out.println(ex);
        }

        //распарсили строку в массив, исключая запятые
        String[] stringArr = line.split(",");

        //создание объекта типа ComporatorGrowth для наследования метода класса
        Comparator<Integer> growth = new ComporatorGrowth();

        //создание коллекции TreeSet с параметром growth, для применения указанной сортировки
        SortedSet<Integer> numberSetGrowth = new TreeSet<Integer>(growth);

        //преобразование массива типа String в Integer
        Integer numArr[] = new Integer[stringArr.length];
        for (int i = 0; i < stringArr.length; i++) {
            numArr[i] = Integer.parseInt(stringArr[i]);
        }

        //заполнение коллекции элементами массива
        for (int i = 0; i < stringArr.length; i++) {
            numberSetGrowth.add(numArr[i]);
        }

        //вывод коллекции элементов по возрастанию
        System.out.println("Последовательность чисел по возрастанию:");
        System.out.println(numberSetGrowth + "\n");

        Comparator<Integer> decay = new ComporatorDecay();

        SortedSet<Integer> numberSetDecay = new TreeSet<Integer>(decay);

        //заполнение коллекции элементами массива
        for (int i = 0; i < stringArr.length; i++) {
            numberSetDecay.add(numArr[i]);
        }

        //вывод коллекции элементов по убыванию
        System.out.println("Последовательность чисел по убыванию:");
        System.out.println(numberSetDecay + "\n");
    }
}





