package com.company;

import java.util.Comparator;

//для того, чтобы объекты можно было отсортировать применяем интерфейс Comparable,
// в котором создаем свое правило для сортировки
public class ComporatorGrowth implements Comparator<Integer> {

    //переопределение метода для создания своего правила сортировки
    @Override
    public int compare(Integer o1, Integer o2) {
        return o1 - o2;
    }
}


